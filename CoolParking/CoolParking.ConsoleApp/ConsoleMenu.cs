﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoolParking.ConsoleApp
{
    internal class ConsoleMenu
    {
        private ConsoleMenu prevMenu;
        private List<ConsoleMenu> submenus;

        public string Caption { get; private set; }
        private readonly Action menuAction;

        public void Invoke()
        {
            menuAction?.Invoke();
        }

        public ConsoleMenu(string caption, Action action)
        {
            prevMenu = null;
            Caption = caption;
            menuAction = action;
            submenus = new List<ConsoleMenu>();
        }

        public ConsoleMenu(string caption, Action action, params ConsoleMenu[] submenus):this(caption,action)
        {
            AddSubmenus(submenus);
        }

        public void AddSubmenus(params ConsoleMenu[] submenus)
        {
            if (submenus != null)
            {
                submenus.ToList().ForEach(sm => sm.prevMenu = this);
                this.submenus.AddRange(submenus);
            }
        }

        public void RemoveSubmenu(ConsoleMenu submenu)
        {
            submenus.Remove(submenu);
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(Caption);
            for(var i=0; (submenus!= null ) && (i < submenus.Count) ; ++i)
            {
                stringBuilder.AppendLine($"{i+1,-5}: {submenus[i].Caption,-20}");
            }

            string prevMenuCaption = (prevMenu == null) ? "Exit" : prevMenu.Caption;
            stringBuilder.AppendLine($"{0,-5}: {prevMenuCaption,-20}");

            return stringBuilder.ToString();
        }

        public virtual ConsoleMenu GetSubmenuByNumber(int menuNumber)
        {
            if(menuNumber == 0)
            {
                return this.prevMenu;
            }
            ConsoleMenu menu;
            try
            {
                menu = this.submenus[menuNumber-1];
            }
            catch(IndexOutOfRangeException ex)
            {
                throw new ArgumentException("Wrong menu number", ex);
            }
            return menu;
        }
    }
}
