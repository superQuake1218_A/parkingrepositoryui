﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    class RateSettings
    {
        private Dictionary<VehicleType, decimal> parkingRateDictionary;
        public RateSettings()
        {
            parkingRateDictionary = new Dictionary<VehicleType, decimal>();
        }
        public RateSettings(Dictionary<VehicleType, decimal> parkingRateDictionary)
        {
            this.parkingRateDictionary = parkingRateDictionary;
        }
        public void AddRate(VehicleType vehicleType,decimal rate)
        {
            parkingRateDictionary.Add(vehicleType, rate);
        }
        public decimal GetRate(VehicleType vehicleType)
        {
            return parkingRateDictionary[vehicleType];
        }
        public void RemoveRate(VehicleType vehicleType)
        {
            parkingRateDictionary.Remove(vehicleType);
        }
    }
}
