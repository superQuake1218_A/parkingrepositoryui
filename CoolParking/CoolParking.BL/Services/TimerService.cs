﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get => timer.Interval; set => timer.Interval = value; }

        public event ElapsedEventHandler Elapsed;
        private readonly Timer timer;

        public TimerService()
        {
            timer = new Timer();
            timer.Elapsed += OnElapsed;
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        protected void OnElapsed(object sender, ElapsedEventArgs args)
        {
            Elapsed?.Invoke(this, null);
        }

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}