﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
namespace CoolParking.BL.Services
{
    using CoolParking.BL.Interfaces;
    using CoolParking.BL.Models;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class ParkingService : IParkingService
    {
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private readonly ILogService logService;
        private static readonly object lockObj = new object();

        private readonly ConcurrentQueue<TransactionInfo> transactionInfos;

        private Parking parking;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            Settings settings = new Settings();
            settings.StartParkingBalance = 0;
            settings.PlacesCount = 10;
            settings.BillingPeriod = new System.TimeSpan(0, 0, 5);
            settings.LoggingPeriod = new System.TimeSpan(0, 1, 0);
            var rateSettingsDictionary = new Dictionary<VehicleType, decimal>() {
                                                    {VehicleType.PassengerCar,2.0M },
                                                    { VehicleType.Truck, 5.0M },
                                                    { VehicleType.Bus, 3.5M },
                                                    { VehicleType.Motorcycle, 1.0M } };
            settings.RateSettings = new RateSettings(rateSettingsDictionary);
            settings.PenaltyRatio = 2.5M;
            Parking.Settings = settings;

            transactionInfos = new ConcurrentQueue<TransactionInfo>();

            parking = Parking.Instance;
            parking.Clear();

            this.withdrawTimer = withdrawTimer;
            withdrawTimer.Interval = Parking.Settings.BillingPeriod.TotalMilliseconds;
            withdrawTimer.Elapsed += (e, a) => WithdrawPaymentParking();

            this.logTimer = logTimer;
            logTimer.Interval = Parking.Settings.LoggingPeriod.TotalMilliseconds;
            logTimer.Elapsed += (e, a) => WriteTransactionsToLog();

            this.logService = logService;

            withdrawTimer.Start();
            logTimer.Start();
        }

        public void StopService()
        {
            withdrawTimer?.Stop();
            logTimer?.Stop();
        }

        public void StartService()
        {
            withdrawTimer?.Start();
            logTimer?.Start();
        }

        private decimal WithdrawPayment(Vehicle vehicle)
        {
            decimal sum = 0;
            lock (lockObj)
            {
                var rate = Parking.Settings.RateSettings.GetRate(vehicle.VehicleType);
                var balance = vehicle.Balance;
                
                if (balance >= rate)
                {
                    sum = rate;
                }
                else
                {
                    if (balance > 0)
                    {
                        var deficit = Math.Abs(balance - rate);
                        sum = balance + deficit * Parking.Settings.PenaltyRatio;
                    }
                    else
                    {
                        sum = rate * Parking.Settings.PenaltyRatio;
                    }
                }

                vehicle.ChangeBalance(-sum);
            }
            return sum;
        }

        private void WithdrawPaymentParking()
        {
            foreach(var vehicle in parking.GetVehicles())
            {
                decimal sum = WithdrawPayment(vehicle);
                parking.Balance += sum;

                var transaction = new TransactionInfo(DateTime.Now,vehicle.Id,sum);
                transactionInfos.Enqueue(transaction);
            }
        }

        private void WriteTransactionsToLog()
        {
            while (!transactionInfos.IsEmpty)
            {
                TransactionInfo transaction;
                if(transactionInfos.TryDequeue(out transaction))
                {
                    logService.Write($"{transaction.Time} {transaction.VehicleId} {transaction.Sum}");
                }
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            parking.AddVehicle(vehicle);
        }

        public void Dispose()
        {
            withdrawTimer.Stop();
            logTimer.Stop();

            withdrawTimer.Dispose();
            logTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return parking.GetCapacity();
        }

        public int GetFreePlaces()
        {
            return parking.GetFreePlaces();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfos.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.GetVehicles();
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            lock(lockObj)
            {
                if (parking.GetVehicle(vehicleId).Balance < 0)
                {
                    throw new InvalidOperationException("Negative balance");
                }
                parking.RemoveVehicle(vehicleId);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if(sum < 0)
            {
                throw new ArgumentException("Added sum is negative!");
            }
            parking.TopUpVehicle(vehicleId, sum);
        }
    }
}

